# Base code from https://learn.hashicorp.com/tutorials/terraform/aws-variables?in=terraform/aws-get-started

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "Rhombus Power DevOps Challenge Instance"
}

variable "default_public_key" {
  description = "Public Key of Default User"
  type        = string
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCpB/Stvfjv00c2JIvjn7hEkB4DSQae4JVA2ahb4tRzQY4EaistVQ8VwbttoTGkLVKqYiIoUtgw0gxHP0MpP3kccWN+Rv57kFLCQWi+ZY72mRwh3e5qma6PVwuJRBYR+e5I5Osx3X+ESsfJFS16dRHNGp1g6pKJrw6ka9Z4w2UmlbIz/4tfOC/25WD3uhxW65+yss/uvbPsOfI4EJ4kFhfVUc9M4voTPW9reI6eMtuUV93GTCHiy+ajl2izxLJ8w3n5I+WqUXBSMg/89ucUZUnfFgBeYOfZ6NcDBy2GNPDrgP5tM4TuaSyTPdowlalql8SSY5gfNUkCZ0GNtj4yHSAlYS6tvKJmwC1kNmRL6R2T8N1pogmyZZKoXWCkhFYkS8PJdks/EhEIwTlJfg1BtRQW6jaHVBxUnBV7kdhIhc68BrXC2F7HBvwncFkWus8NVC6NTLk3bj/4q+tfURkbZwyML9107+FE/PU44Go8wtQoM3E1K8jy27vpaeRXpS/fUOc="
}