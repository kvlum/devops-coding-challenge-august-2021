# Base code from https://learn.hashicorp.com/tutorials/terraform/aws-build
# Provisioning base code from https://github.com/hashicorp/learn-terraform-provisioning/blob/cloudinit/instances/main.tf

terraform {

  /*
  # Block for enabling remote storage of state
  backend "remote" {
    organization = "kvlum"
    workspaces {
      name = "" # Add name of workspace here.
    }
  }
 */

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.56.0"
    }
  }

  required_version = ">= 1.0.5"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

# Base code from https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id = aws_vpc.vpc.id

  ingress {
    description      = "Allow all SSH traffic"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  # Allow all outbound traffic
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

# Key pair for the default user of the EC2 instance.
# Base code from https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair
resource "aws_key_pair" "ec2_user_key" {
  key_name   = "default-user-key"
  public_key = var.default_public_key
}

# Provides a VPC resource.
# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc
resource "aws_vpc" "vpc" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true			# The enable_dns_support argument defaults to true.
  enable_dns_hostnames = true			# The enable_dns_hostnames argument defaults to false.
}

# Provides a VPC Internet Gateway
# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
}

# Provides a VPC Subnet
# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet
resource "aws_subnet" "subnet_public" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = "10.1.0.0/24"
}

# Provides a VPC route table.
# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table
resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

# Associates a route table with a subnet or internet gateway.
# Documetation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association
resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.rtb_public.id
}

data "template_file" "user_data" {
  template = file("Provision Instance.yaml")
}

resource "aws_instance" "app_server" {
  ami           = "ami-083ac7c7ecf9bb9b0" # Amazon Linux 2 AMI (HVM), SSD Volume Type (64-bit x86) instance in "us-west-2" region
  instance_type = "t2.micro"
  # security_groups             = ["default"]
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name               = aws_key_pair.ec2_user_key.key_name

  user_data                   = data.template_file.user_data.rendered
  subnet_id                   = aws_subnet.subnet_public.id
  associate_public_ip_address = true

  tags = {
    Name = var.instance_name
  }
}
